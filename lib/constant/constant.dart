/*
this is constant pages
 */

import 'package:flutter/material.dart';

const int STATUS_OK = 200;
const int STATUS_CREATED_OK = 201;
const int STATUS_NO_CONTENT = 204;
const int STATUS_BAD_REQUEST = 400;
const int STATUS_UNAUTHORIZED = 401;
const int STATUS_NOT_AUTHORIZED = 403;
const int STATUS_NOT_FOUND = 404;
const int STATUS_INTERNAL_ERROR = 500;
const String SERVER_URL = "https://api.opendota.com/api/herostats";
const String SERVER_URL_IMAGE = "https://api.opendota.com";
const Color colorPrimary = Color(0xFF373737);
const Color colorCard = Color(0xff252525);
