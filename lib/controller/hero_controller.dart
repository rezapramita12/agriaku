import 'package:getx/models/response/response_hero.dart';
import 'package:getx/services/hero_api.dart';
import 'package:get/get.dart';

class HerosController extends GetxController {
  var isLoading = true.obs;
  var state;
  // var listHero = List<ResponseHero>().obs;
  List<ResponseHero> listHero = [];

  List<ResponseHero> get listHeroEscape =>
      listHero.where((hero) => hero.roles.contains(Role.ESCAPE)).toList();
  List<ResponseHero> get listHeroDisabler =>
      listHero.where((hero) => hero.roles.contains(Role.DISABLER)).toList();
  List<ResponseHero> get listHeroInitiator =>
      listHero.where((hero) => hero.roles.contains(Role.INITIATOR)).toList();
  List<ResponseHero> get sortAgi =>
      listHero..sort((a, b) => b.moveSpeed.compareTo(a.moveSpeed));
  List<ResponseHero> get sortInt =>
      listHero..sort((a, b) => b.baseMana.compareTo(a.baseMana));
  List<ResponseHero> get sortStr =>
      listHero..sort((a, b) => b.baseAttackMax.compareTo(a.baseAttackMax));
  List<ResponseHero> get listHeroAgi => sortAgi
      .where(
          (hero) => hero.primaryAttr.toString() == PrimaryAttr.AGI.toString())
      .toList();
  List<ResponseHero> get listHeroInt => sortInt
      .where(
          (hero) => hero.primaryAttr.toString() == PrimaryAttr.INT.toString())
      .toList();
  List<ResponseHero> get listHeroStr => sortStr
      .where(
          (hero) => hero.primaryAttr.toString() == PrimaryAttr.STR.toString())
      .toList();
  onInit() {
    getHero();
    super.onInit();
  }

  Future<List<ResponseHero>> getHero() async {
    isLoading(true);
    state = 'loading';
    try {
      var hero = await HeroAPI.getHero();
      if (hero != null) {
        state = 'hasData';
        listHero = hero;
      } else {
        state = null;
      }
    } finally {
      isLoading(false);
    }
  }
}
