import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:getx/constant/constant.dart';
import 'package:getx/controller/hero_controller.dart';
import 'package:getx/models/response/response_hero.dart';
import 'package:string_extensions/string_extensions.dart';

class DetailView extends StatefulWidget {
  final ResponseHero responseHero;
  const DetailView({Key key, this.responseHero}) : super(key: key);

  @override
  _DetailViewState createState() => _DetailViewState();
}

class _DetailViewState extends State<DetailView> {
  HerosController heroController = Get.put(HerosController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorPrimary,
      appBar: AppBar(
        backgroundColor: colorCard,
        title: Center(child: Text(widget.responseHero.localizedName)),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: 200,
            alignment: Alignment.bottomLeft,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                        SERVER_URL_IMAGE + widget.responseHero.img),
                    fit: BoxFit.cover),
                border: Border.all(width: 1, color: Colors.black)),
            child: SizedBox(
              height: 20,
              child: Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: ListView.builder(
                    itemCount: widget.responseHero.roles.length,
                    scrollDirection: Axis.horizontal,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      var role = widget.responseHero.roles[index];
                      return Text(
                        role.toString().toTitleCase().split(".").last + ",",
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                        overflow: TextOverflow.ellipsis,
                      );
                    }),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Card(
              color: colorCard,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                children: [
                  cardContent(
                      'Type',
                      widget.responseHero.attackType
                          .toString()
                          .toTitleCase()
                          .split(".")
                          .last),
                  cardContent(
                      'Max Atk',
                      widget.responseHero.baseAttackMin.toString() +
                          "-" +
                          widget.responseHero.baseAttackMax.toString()),
                  cardContent(
                      'Health', widget.responseHero.baseHealth.toString()),
                  cardContent('MSPD', widget.responseHero.moveSpeed.toString()),
                  cardContent(
                      'Attr',
                      widget.responseHero.primaryAttr
                          .toString()
                          .toTitleCase()
                          .split(".")
                          .last),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(16),
            child: Text(
              "Similar Heros",
              style: TextStyle(color: Colors.grey, fontSize: 17),
            ),
          ),
          GetBuilder(
              init: HerosController(),
              builder: (controller) {
                return widget.responseHero.primaryAttr.toString() ==
                        PrimaryAttr.AGI.toString()
                    ? Expanded(
                        child: StaggeredGridView.countBuilder(
                            crossAxisCount: 1,
                            itemCount: heroController.listHeroAgi.length -
                                heroController.listHeroAgi.length +
                                3,
                            crossAxisSpacing: 5,
                            mainAxisSpacing: 5,
                            itemBuilder: (context, index) {
                              var hero = heroController.listHeroAgi[index];

                              return Container(
                                padding: const EdgeInsets.all(16.0),
                                margin: EdgeInsets.all(16),
                                height: 100,
                                width: MediaQuery.of(context).size.width * 0.9,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    image: DecorationImage(
                                        image: NetworkImage(
                                            SERVER_URL_IMAGE + hero.img),
                                        colorFilter: new ColorFilter.mode(
                                            Colors.black.withOpacity(0.2),
                                            BlendMode.dstATop),
                                        fit: BoxFit.cover)),
                                child: Center(
                                    child: Text(
                                  hero.localizedName,
                                  style: TextStyle(color: Colors.white),
                                )),
                              );
                            },
                            staggeredTileBuilder: (index) =>
                                StaggeredTile.fit(1)),
                      )
                    : widget.responseHero.primaryAttr.toString() ==
                            PrimaryAttr.INT.toString()
                        ? Expanded(
                            child: StaggeredGridView.countBuilder(
                                crossAxisCount: 1,
                                itemCount: heroController.listHeroInt.length -
                                    heroController.listHeroInt.length +
                                    3,
                                crossAxisSpacing: 5,
                                mainAxisSpacing: 5,
                                itemBuilder: (context, index) {
                                  var hero = heroController.listHeroInt[index];

                                  return Container(
                                    padding: const EdgeInsets.all(16.0),
                                    margin: EdgeInsets.all(16),
                                    height: 100,
                                    width:
                                        MediaQuery.of(context).size.width * 0.9,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        image: DecorationImage(
                                            image: NetworkImage(
                                                SERVER_URL_IMAGE + hero.img),
                                            colorFilter: new ColorFilter.mode(
                                                Colors.black.withOpacity(0.2),
                                                BlendMode.dstATop),
                                            fit: BoxFit.cover)),
                                    child: Center(
                                        child: Text(
                                      hero.localizedName,
                                      style: TextStyle(color: Colors.white),
                                    )),
                                  );
                                },
                                staggeredTileBuilder: (index) =>
                                    StaggeredTile.fit(1)),
                          )
                        : Expanded(
                            child: StaggeredGridView.countBuilder(
                                crossAxisCount: 1,
                                itemCount: heroController.listHeroStr.length -
                                    heroController.listHeroStr.length +
                                    3,
                                crossAxisSpacing: 5,
                                mainAxisSpacing: 5,
                                itemBuilder: (context, index) {
                                  var hero = heroController.listHeroStr[index];

                                  return Container(
                                    padding: const EdgeInsets.all(16.0),
                                    margin: EdgeInsets.all(16),
                                    height: 100,
                                    width:
                                        MediaQuery.of(context).size.width * 0.9,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        image: DecorationImage(
                                            image: NetworkImage(
                                                SERVER_URL_IMAGE + hero.img),
                                            colorFilter: new ColorFilter.mode(
                                                Colors.black.withOpacity(0.2),
                                                BlendMode.dstATop),
                                            fit: BoxFit.cover)),
                                    child: Center(
                                        child: Text(
                                      hero.localizedName,
                                      style: TextStyle(color: Colors.white),
                                    )),
                                  );
                                },
                                staggeredTileBuilder: (index) =>
                                    StaggeredTile.fit(1)),
                          );
              })

          // Expanded(
          //     child: Obx(() => StaggeredGridView.countBuilder(
          //         crossAxisCount: 1,
          //         itemCount: sortDataAgi.length - sortDataAgi.length + 3,
          //         crossAxisSpacing: 5,
          //         mainAxisSpacing: 5,
          //         itemBuilder: (context, index) {
          //           var listHeroAgi = sortDataAgi[index];
          //           var listHeroStr = sortDataStr[index];
          //           var listHeroInt = sortDataInt[index];

          //           return listHeroAgi.primaryAttr.toString().split(".").last ==
          //                   "AGI"
          //               ? Container(
          //                   padding: const EdgeInsets.all(16.0),
          //                   margin: EdgeInsets.all(16),
          //                   height: 100,
          //                   width: MediaQuery.of(context).size.width * 0.9,
          //                   decoration: BoxDecoration(
          //                       borderRadius: BorderRadius.circular(20),
          //                       image: DecorationImage(
          //                           image: NetworkImage(
          //                               SERVER_URL_IMAGE + listHeroAgi.img),
          //                           colorFilter: new ColorFilter.mode(
          //                               Colors.black.withOpacity(0.2),
          //                               BlendMode.dstATop),
          //                           fit: BoxFit.cover)),
          //                   child: Center(
          //                       child: Text(
          //                     listHeroAgi.localizedName,
          //                     style: TextStyle(color: Colors.white),
          //                   )),
          //                 )
          //               : listHeroInt.primaryAttr.toString().split(".").last ==
          //                       "INT"
          //                   ? Container(
          //                       padding: const EdgeInsets.all(16.0),
          //                       margin: EdgeInsets.all(16),
          //                       height: 100,
          //                       width: MediaQuery.of(context).size.width * 0.9,
          //                       decoration: BoxDecoration(
          //                           borderRadius: BorderRadius.circular(20),
          //                           image: DecorationImage(
          //                               image: NetworkImage(
          //                                   SERVER_URL_IMAGE + listHeroInt.img),
          //                               colorFilter: new ColorFilter.mode(
          //                                   Colors.black.withOpacity(0.2),
          //                                   BlendMode.dstATop),
          //                               fit: BoxFit.cover)),
          //                       child: Center(
          //                           child: Text(
          //                         listHeroInt.localizedName,
          //                         style: TextStyle(color: Colors.white),
          //                       )),
          //                     )
          //                   : listHeroStr.primaryAttr
          //                               .toString()
          //                               .split(".")
          //                               .last ==
          //                           "STR"
          //                       ? Container(
          //                           padding: const EdgeInsets.all(16.0),
          //                           margin: EdgeInsets.all(16),
          //                           height: 100,
          //                           width:
          //                               MediaQuery.of(context).size.width * 0.9,
          //                           decoration: BoxDecoration(
          //                               borderRadius: BorderRadius.circular(20),
          //                               image: DecorationImage(
          //                                   image: NetworkImage(
          //                                       SERVER_URL_IMAGE +
          //                                           listHeroStr.img),
          //                                   colorFilter: new ColorFilter.mode(
          //                                       Colors.black.withOpacity(0.2),
          //                                       BlendMode.dstATop),
          //                                   fit: BoxFit.cover)),
          //                           child: Center(
          //                               child: Text(
          //                             listHeroStr.localizedName,
          //                             style: TextStyle(color: Colors.white),
          //                           )),
          //                         )
          //                       : Container();
          //         },
          //         staggeredTileBuilder: (index) => StaggeredTile.fit(1))))
        ],
      ),
    );
  }

  Container cardContent(dynamic title, dynamic content) {
    return Container(
      padding: EdgeInsets.all(13),
      child: Column(
        children: [
          Text(
            title,
            style: TextStyle(color: Colors.grey),
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            content,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
