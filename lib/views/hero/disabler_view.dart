import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:getx/constant/constant.dart';
import 'package:getx/controller/hero_controller.dart';
import 'package:getx/models/response/response_hero.dart';
import 'package:string_extensions/string_extensions.dart';
import 'detail_view.dart';

class DisablerHero extends StatelessWidget {
  const DisablerHero({
    Key key,
    @required this.heroController,
  }) : super(key: key);

  final HerosController heroController;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GetBuilder<HerosController>(
          init: HerosController(),
          builder: (controller) {
            return Expanded(
              child: StaggeredGridView.countBuilder(
                  crossAxisCount: 2,
                  itemCount: heroController.listHeroDisabler.length,
                  crossAxisSpacing: 5,
                  mainAxisSpacing: 5,
                  itemBuilder: (context, index) {
                    var hero = heroController.listHeroDisabler[index];

                    return GestureDetector(
                      onTap: () => Get.to(DetailView(responseHero: hero)),
                      child: Container(
                        padding: const EdgeInsets.all(5.0),
                        height: 250,
                        child: Card(
                          color: colorCard,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 100,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: NetworkImage(
                                              SERVER_URL_IMAGE + hero.img),
                                          fit: BoxFit.cover),
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10)))),
                              Padding(
                                padding: EdgeInsets.all(16),
                                child: Text(
                                  hero.localizedName,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                                child: Container(
                                  padding: const EdgeInsets.only(left: 16.0),
                                  width: 150,
                                  child: ListView.builder(
                                      itemCount: hero.roles.length,
                                      scrollDirection: Axis.horizontal,
                                      physics: NeverScrollableScrollPhysics(),
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        var role = hero.roles[index];
                                        return Text(
                                          role
                                                  .toString()
                                                  .toTitleCase()
                                                  .split(".")
                                                  .last +
                                              ",",
                                          style: TextStyle(
                                            color: Colors.grey,
                                          ),
                                          overflow: TextOverflow.ellipsis,
                                        );
                                      }),
                                ),
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 16, top: 10.0),
                                    child: Container(
                                      width: 5,
                                      height: 5,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(100),
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(0.5),
                                              spreadRadius: 1,
                                              blurRadius: 1,
                                              offset: Offset(0,
                                                  0), // changes position of shadow
                                            ),
                                          ],
                                          color: hero.primaryAttr.toString() ==
                                                  PrimaryAttr.STR.toString()
                                              ? Colors.red
                                              : hero.primaryAttr.toString() ==
                                                      PrimaryAttr.AGI.toString()
                                                  ? Colors.green
                                                  : Colors.blue),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: 7, top: 4, bottom: 16),
                                    child: Text(
                                      hero.primaryAttr
                                          .toString()
                                          .toTitleCase()
                                          .split(".")
                                          .last,
                                      style: TextStyle(
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  staggeredTileBuilder: (index) => StaggeredTile.fit(1)),
            );
          },
        ),
      ],
    );
  }
}
