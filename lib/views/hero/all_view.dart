import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:getx/constant/constant.dart';
import 'package:getx/controller/hero_controller.dart';
import 'package:getx/models/response/response_hero.dart';
import 'package:getx/views/hero/detail_view.dart';
import 'package:string_extensions/string_extensions.dart';

class CardHero extends StatelessWidget {
  const CardHero({
    Key key,
    @required this.heroController,
  }) : super(key: key);

  final HerosController heroController;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(child: Obx(() {
          if (heroController.isLoading.value) {
            return Center(child: CircularProgressIndicator());
          } else if (heroController.state == null) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Error",
                  style: TextStyle(fontSize: 17, color: Colors.white),
                ),
                SizedBox(
                  height: 16,
                ),
                Text(
                  "Error Description goes here",
                  style: TextStyle(color: Colors.white),
                ),
                SizedBox(
                  height: 16,
                ),
                RaisedButton(
                  color: colorCard,
                  onPressed: () {
                    heroController.getHero();
                  },
                  child: Text(
                    "Retry",
                    style: TextStyle(color: Colors.white),
                  ),
                )
              ],
            );
          } else {
            return StaggeredGridView.countBuilder(
                crossAxisCount: 2,
                itemCount: heroController.listHero.length,
                crossAxisSpacing: 5,
                mainAxisSpacing: 5,
                itemBuilder: (context, index) {
                  var listHero = heroController.listHero[index];
                  var listRole = listHero.roles;
                  return Container(
                    padding: const EdgeInsets.all(5.0),
                    height: 240,
                    child: GestureDetector(
                      onTap: () => Get.to(DetailView(
                        responseHero: listHero,
                      )),
                      child: Card(
                        color: colorCard,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.network(SERVER_URL_IMAGE + listHero.img),
                            Padding(
                              padding: EdgeInsets.all(16),
                              child: Text(
                                listHero.localizedName,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 16.0),
                                child: ListView.builder(
                                    itemCount: listRole.length,
                                    scrollDirection: Axis.horizontal,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      var role = listRole[index];
                                      return Text(
                                        role
                                                .toString()
                                                .toTitleCase()
                                                .split(".")
                                                .last +
                                            ",",
                                        style: TextStyle(
                                          color: Colors.grey,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      );
                                    }),
                              ),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 16, top: 10.0),
                                  child: Container(
                                    width: 5,
                                    height: 5,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            spreadRadius: 1,
                                            blurRadius: 1,
                                            offset: Offset(0,
                                                0), // changes position of shadow
                                          ),
                                        ],
                                        color: listHero.primaryAttr
                                                    .toString() ==
                                                PrimaryAttr.STR.toString()
                                            ? Colors.red
                                            : listHero.primaryAttr.toString() ==
                                                    PrimaryAttr.AGI.toString()
                                                ? Colors.green
                                                : Colors.blue),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 7, top: 4, bottom: 16),
                                  child: Text(
                                    listHero.primaryAttr
                                        .toString()
                                        .toTitleCase()
                                        .split(".")
                                        .last,
                                    style: TextStyle(
                                      color: Colors.grey,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
                staggeredTileBuilder: (index) => StaggeredTile.fit(1));
          }
        }))
      ],
    );
  }
}
