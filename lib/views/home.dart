import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx/constant/constant.dart';
import 'package:getx/controller/hero_controller.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:getx/views/hero/all_view.dart';
import 'package:getx/views/hero/disabler_view.dart';
import 'package:getx/views/hero/escape_view.dart';
import 'package:getx/views/hero/initiator_view.dart';
import 'package:string_extensions/string_extensions.dart';

class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  HerosController heroController = Get.put(HerosController());
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(initialIndex: 0, length: 4, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(120),
          child: Column(
            children: [
              AppBar(
                backgroundColor: colorCard,
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "img/logo.png",
                      scale: 13,
                    ),
                    Center(
                      child: Text(
                        "DOTA",
                        style: TextStyle(fontSize: 30),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                color: colorCard,
                child: TabBar(
                    tabs: <Widget>[
                      Tab(child: Text("All Heroes")),
                      Tab(child: Text("Disabler")),
                      Tab(child: Text("Initiator")),
                      Tab(child: Text("Escape"))
                    ],
                    indicatorColor: Colors.red,
                    labelColor: Colors.white,
                    unselectedLabelColor: Colors.grey,
                    labelStyle: TextStyle(color: Colors.white),
                    unselectedLabelStyle: TextStyle(color: Colors.grey),
                    controller: _tabController,
                    onTap: (index) {
                      setState(() {
                        _tabController.index = index;
                      });
                    }),
              ),
            ],
          ),
        ),
        backgroundColor: colorPrimary,
        body: TabBarView(controller: _tabController, children: [
          CardHero(heroController: heroController),
          DisablerHero(heroController: heroController),
          InitiatorHero(heroController: heroController),
          EscapeHero(heroController: heroController)
        ]));
  }
}
