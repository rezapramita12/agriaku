import 'dart:convert';

import 'package:getx/constant/constant.dart';
import 'package:getx/models/response/response_hero.dart';
import 'package:http/http.dart' as http;

class HeroAPI {
  static var client = http.Client();

  static Future<List<ResponseHero>> getHero() async {
    final response = await client.get(
      Uri.parse(SERVER_URL),
    );
    if (response.statusCode == STATUS_OK) {
      return responseHeroFromJson(response.body);
    } else {
      return null;
    }
  }
}
